<?php
ini_set('max_execution_time', 150);
ini_set('memory_limit', '1024M');
ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');

header('Access-Control-Allow-Origin: *');
header("Content-Type: application/json");

require_once './bootstrap/app.php';

$app->run();

