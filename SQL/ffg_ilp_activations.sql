-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 08, 2021 at 06:18 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `OctechDigital`
--

-- --------------------------------------------------------

--
-- Table structure for table `ffg_ilp_activations`
--

CREATE TABLE `ffg_ilp_activations` (
  `userKey` bigint(20) UNSIGNED NOT NULL,
  `masterKey` bigint(20) UNSIGNED DEFAULT NULL,
  `device` varchar(30) NOT NULL,
  `user_ip` varchar(30) NOT NULL,
  `browser` varchar(30) DEFAULT NULL,
  `os` varchar(30) DEFAULT NULL,
  `source` varchar(120) NOT NULL DEFAULT 'Social',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_day` varchar(30) NOT NULL,
  `created_hour` int(10) UNSIGNED NOT NULL,
  `created_day_hour` varchar(30) NOT NULL,
  `created_date` date DEFAULT NULL,
  `submit_commitment` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `submit_initiative` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ffg_ilp_activations`
--
ALTER TABLE `ffg_ilp_activations`
  ADD PRIMARY KEY (`userKey`),
  ADD KEY `created_date` (`created_date`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
