<?php
require __DIR__.'/../vendor/autoload.php';

use Slim\Http\UploadedFile;

$app = new \Slim\App([
    'setting'=> [
	    'displayErrorDetails'=>true,
        'determineRouteBeforeAppMiddleware' => true,
        'db' => [
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'iplanetdev',
            'username' => 'iplanetdev',
            'password' => 'M*3aFw2b83E>`j=j',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => 'ffg_ilp_',
        ],
        'renderer'            => [
            'blade_template_path' => __DIR__ . '/../views/admin/', // String or array of multiple paths
            'blade_cache_path'    => __DIR__ . '/../cache', // Mandatory by default, though could probably turn caching off for development
        ],
    ]
]);
$container = $app->getContainer();


$container['view'] = function ($container) {
    return new \Slim\Views\Blade(
        $container['settings']['renderer']['blade_template_path'],
        $container['settings']['renderer']['blade_cache_path']
    );
};
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['setting']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();
$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $response
            ->write('eyJzdGF0dXNDb2RlIjo0MDQsICJtZXNzYWdlIjoiSW52YWxpZCByZXF1ZXN0In0=');
    };
};

// $container['video_directory'] = __DIR__ . '/../../videos';
$container['video_directory'] = __DIR__ . '/videos';
// $container['dp_directory'] = __DIR__ . '/../../dp/';
$container['dp_directory'] = __DIR__ . '/uploads';
$container['thumbnail_directory'] = __DIR__ . '/../../thumbnail/';

date_default_timezone_set("Asia/Kolkata");

$container['db'] = function ($container) use ($capsule) {
    return $capsule;
};

$container["UsersController"] = function($container){
    
    return new \App\Controllers\UsersController($container);
};

$container["DashboardController"] = function($container){
    
    return new \App\Controllers\DashboardController($container);
};

require __DIR__ . './../app/routes.php';


//$app->response->headers->set('Access-Control-Allow-Origin', '*');
//$app->response->headers->set('Access-Control-Allow-Methods', '*');
//$app->response->headers->set('Access-Control-Allow-Headers', '*');
$app->run();
