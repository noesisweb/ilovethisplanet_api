<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class BaseModel extends Model
{
    public static function createUser($device = 'web', $values = []){

        $device =($device=="mobile")?"mobile":"web";
        $child_name = get_called_class();
        $userKey=0;
        $max=mt_getrandmax();
        if($max>4294967290)
            $max=4294967290;
        while($userKey==0){
            $userKey = mt_rand(1,$max);
            $flag = $child_name::isUnique('userKey', $userKey);
            if(!$flag)
            {
                $userKey=0;
            }
        }
        $values["userKey"] = $userKey;
        if(!(isset($values["masterKey"]) && $values["masterKey"] != "")){
            $values["masterKey"] = $userKey;
        }
        $values["device"] = $device;
        $values["user_ip "] = $child_name::getClientIp();
        $browser = get_browser(null, true);
        $values["browser"] = $browser['browser'];
        $values["os"] = $browser['platform'];
        //$values["browser_data"] = json_encode($browser);
        $values["created_day"] = date("l");
        $values["created_hour"] = intval(date("G"));
        $values["created_day_hour"] = date("D-G");
        //echo json_encode($values);
        $resp = $child_name::saveData($values);
        if($resp["code"]==200)
            return $userKey;
        else
            return 0;
    }
    public static function saveData($data = []){

        if(is_array($data) && count($data)>0){
            $child_name = get_called_class();
            $obj = new $child_name();
            foreach($data as $key=>$val){
                $encoded_key = $child_name::htmlEncode($key);
                $obj->$encoded_key = $child_name::htmlEncode($val);
            }
            if($obj->save()){
                return array("code"=>200,"message"=>"Data saved", "id" => $obj->id);
            }
            else{
                return array("code"=>401,"message"=>"Invalid data format");
            }
        }
        return array("code"=>400,"message"=>"Invalid data format");
    }
    public static function clickUpdater($userKey = 0, $col_name = "", $col_on = 'userKey'){
        $child_name = get_called_class();
        $userKey = $child_name::htmlEncode($userKey);
        $col_name = $child_name::htmlEncode($col_name);
        $col_on = $child_name::htmlEncode($col_on);
        if($userKey== 0)
            return false;
        if($col_name!=''){
            $child_name::query()
                ->where($col_on, $userKey)
                ->increment($col_name);
            return true;
        }
        return false;
    }
    public static function clickActivator($userKey = 0, $col_name = "", $col_on = 'userKey'){
        $child_name = get_called_class();
        $userKey = $child_name::htmlEncode($userKey);
        $col_name = $child_name::htmlEncode($col_name);
        $col_on = $child_name::htmlEncode($col_on);
        if($userKey== 0)
            return false;
        if($col_name!=''){
            $child_name::query()
                ->where($col_on, $userKey)
                ->update([$col_name => 1]);
            return true;
        }
        return false;
    }
    public static function isUnique($col_name = "", $value = ""){
        $child_name = get_called_class();
        $col_name = $child_name::htmlEncode($col_name);
        $value = $child_name::htmlEncode($value);
        if($col_name == '' || $value == '')
            return false;
        $count = $child_name::where($col_name, $value)->count();
        if($count == 0)
            return true;
        return false;
    }
    public static function htmlEncode($data){
        if(is_array($data)){
            foreach($data as $key=>$val){
                $data[htmlspecialchars(trim($key),ENT_QUOTES,'UTF-8')]=htmlspecialchars(trim($val),ENT_QUOTES,'UTF-8');
            }
            return $data;
        }
        else{
            return htmlspecialchars(trim($data),ENT_QUOTES,'UTF-8');
        }
    }
    public static function getClientIp() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    public static function isEmail($email = ""){
        if (filter_var($email, FILTER_VALIDATE_EMAIL))
            return true;
        return false;
    }
    public static function isNumber($number = 0, $min = 0 , $max = 50, $start_with_plus = false){
        if($number!="" && $number["0"]==0)
            $number = substr($number,1);
        if (filter_var($number, FILTER_VALIDATE_INT) && $number>=0 && !(!$start_with_plus && $number[0]=="+")){
            if(strlen($number) >= $min && strlen($number) <= $max){
                return true;
            }
        }
        return false;
    }
    private static function validatedData($data = []){
        $child_name = get_called_class();
        if(is_array($data)){
            $values =  array();
            foreach($data as $field){
                if(is_array($data) && isset($field["name"]) && isset($field["value"])){
                    $type = isset($field["type"])?$field["type"]:"text";
                    switch ($type){
                        case "email":
                            if((!isset($field["required"]) || $field["required"]!=true) && trim($field["value"])==""){
                                $values[$field["name"]] = $field["value"];
                            }
                            else{
                                if($child_name::isEmail($field["value"])){
                                    $values[$field["name"]] = $field["value"];
                                }
                                else
                                    return array("code"=>406,"message"=>"Invalid ".$field["name"]);
                            }
                            break;
                        case "number":
                            $min = isset($field["minLength"])?$field["minLength"]:0;
                            $max = isset($field["maxLength"])?$field["maxLength"]:50;
                            if(isset($field["required"]) && $field["required"]==true && $min==0)
                                $min=1;
                            $start_with_plus = isset($field["start_with_plus"])?$field["start_with_plus"]:false;
                            if($min>0 || $field["value"]!=""){
                                if($child_name::isNumber($field["value"],$min,$max,$start_with_plus)){
                                    $values[$field["name"]] = $field["value"];
                                }
                                else
                                    return array("code"=>406,"message"=>"Invalid ".$field["name"]);
                            }
                            else
                                break;
                        default:
                            if(isset($field["required"]) && $field["required"]==true){
                                if(trim($field["value"])=="")
                                    return array("code"=>406,"message"=>$field["name"]." can not be empty.");
                            }
                            if(isset($field["minLength"])){
                                if(strlen($field["value"])<$field["minLength"])
                                    return array("code"=>406,"message"=>$field["name"]." should be at least ".$field["minLength"]." character long");
                            }
                            if(isset($field["maxLength"])){
                                if(strlen($field["value"])>$field["maxLength"])
                                    return array("code"=>406,"message"=>$field["name"]." cannot have more than ".$field["maxLength"]." character");
                            }
                            $values[$field["name"]] = $field["value"];
                    }
                    if(isset($field["unique"]) && $field["unique"]==true){
                        if(!$child_name::isUnique($field["name"], $field["value"]))
                            return array("code"=>412,"message"=>$field["name"]." already registered");
                    }
                }
                else
                    return array("code"=>400,"message"=>"Invalid data format.3");
            }
            return array("code"=>200,"values"=>$values);
        }
        return array("code"=>400,"message"=>"Invalid data format.4");
    }
    public static function saveValidatedData($data = []){
        $child_name = get_called_class();
        $resp = $child_name::validatedData($data);
        if($resp["code"]==200)
            return $child_name::saveData($resp["values"]);
        else
            return $resp;
    }
    public static function updateData($data = [],$condition = []){
        $set =  array();
        $child_name = get_called_class();
        $updateCondition = [];
        if(is_array($data) && count($data)>0){
            if(is_array($condition)){
                /* for($i=0; $i<count($condition); $i++){
                    $updateCondition[$i][count($coaendition[$i])-1] = $child_name::htmlEncode($condition[$i][count($condition[$i])-1]);
                } */
                foreach($condition as $key => $val){
                    $updateCondition[] = [$child_name::htmlEncode($key), $child_name::htmlEncode($val)];
                }
                //echo json_encode($updateCondition);
                //return array("code"=>416,"message"=>"Invalid condition");
                $count = $child_name::where($updateCondition)->count();
                if($count!=1){
                    return ["code"=>416,"message"=>"Invalid condition"];
                }
                $vals = [];
                foreach($data as $key=>$val){
                     $encoded_key = $child_name::htmlEncode($key);
                     $vals[$encoded_key] = $child_name::htmlEncode($val);
                 }
                $child_name::where($condition)->update($vals);
                return array("code"=>200,"message"=>"Data updated");
            }
        }
        return array("code"=>400,"message"=>"Invalid data format.5");
    }
    public static function updateValidatedData($data = [],$condition = []){
        $child_name = get_called_class();
        $resp = $child_name::validatedData($data);
        if($resp["code"]==200)
            return $child_name::updateData($resp["values"],$condition);
        else
            return $resp;
    }
    public static function getData($list = [],$key = ""){
        if(is_array($list))
        {
            if(isset($list[$key]))
                return $list[$key];
        }
        return "";
    }
    public static function getToken($min = 8,$max = 8, $possible = ''){
        if($possible=='')
            $possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        if($min==$max)
            $l=$min;
        else
            $l= mt_rand($min,$max);
        $str = "";
        for($i=0; $i<$l;$i++){
            $k= mt_rand(0,(strlen($possible)-1));
            $str .= $possible[$k];
        }
        return $str;
    }
}