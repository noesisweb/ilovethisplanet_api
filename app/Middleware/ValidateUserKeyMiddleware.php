<?php
namespace App\Middleware;

use App\Models\Activation;

class ValidateUserKeyMiddleware
{
    
    public function __invoke($request, $response, $next)
    {
        $isValid = false;

        $route = $request->getAttribute('route');
        $userKey = $route->getArgument('userKey');

        if($userKey && !is_nan($userKey)){
            $activationData = Activation::where("userKey", $userKey)->first();
            if($activationData){
                $request = $request->withAttribute('userKey', $userKey);
                $request = $request->withAttribute('activationData', $activationData);
                $isValid = true;
            }
        }

        if($isValid){
            return $next($request, $response);
        }
        $response->getBody()->write(json_encode(["statusCode" => 400, "message" => "Invalid Data"]));
        return $response;
    }
}