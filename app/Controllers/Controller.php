<?php
namespace App\Controllers;

class Controller
{
    protected $container;

    ## protected $fName_key = "95d216f698432dac26bd555caf55596d4e8b96bb1d97afe3fe754836eeb95862";
    protected $name_key = "096138d976f5dfb1b59aae3cca8bd4ed33de2542a7cfdb1f1fe5f4b16b2ae5a9";
    protected $email_key = "85497abcc36dd3e0864ebdc7724456a9b454e2faf9cd677831ce0e06c0f9735a";
    protected $mobile_key = "158c532be997e4c05a84d91a9b232b9cba590058e35476dd0524b740ddae842f";
    protected $otp_key = "c0bcf00dc5d7f714c5b74f4a74ae2c1c6c0e5be2885ef1d8e6eb4600c24cd146";
    protected $start_date = "2021-08-20";
    
    public function __construct($container){
        $this->container = $container;
    }

    protected function getData($list = [],$key = ""){
        if(is_array($list) && isset($list[$key]))
        {
            return trim($list[$key]);
        }
        return ""; 
    }

    protected function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

    protected function encodeOutput($output, $other = []){
        
        if($other){
            foreach($other as $key => $val){
                $output[$key] = $val;
            }
        }
        return json_encode($output);
        ## return base64_encode(json_encode($output));
    }
}