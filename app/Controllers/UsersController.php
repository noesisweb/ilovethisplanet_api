<?php

namespace App\Controllers;
use Slim\Http\UploadedFile;

use App\Models\Activation;
use App\Models\Commitment;
use App\Models\User;
use App\Models\Video;
use App\Models\Like;
use \View;

class UsersController extends Controller{
    const INVALID_DATA = "Invalid Data";
    const INVALID_MOBILE = "Invalid Mobile Number";
    const MOBILE_REGEX = '/^[56789][0-9]{9}$/';
    
    public function response_header(){
		header('Access-Control-Allow-Origin: *'); 
	    header("Access-Control-Allow-Credentials: true");
	    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
	    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
    }
    
    public function login($req, $res){
	    $this->response_header();
	    $input = $req->getParsedBody();
        $email = $input['email'];
        $password = $input['password'];
        $data = User::select('id','name','mobile','email','password','token')->where('email',$email)->first();
        if (password_verify( $password, $data['password'])) {
            return $this->encodeOutput(["statusCode" => 200, "message" => "Login Successfully","data" => $data]);
        } else {
            return $this->encodeOutput(["statusCode" => 400, "error" => "Invalid password"]);
        }
    }
    public function getUser($req, $res){
	   	$this->response_header();
        //$cust = json_decode($req->getBody());
        $id = $req->getAttribute('id');
        $data = User::select('id','name','email','mobile')->where('id',$id)->first();
        return $this->encodeOutput(["statusCode" => 200,"data" => $data]);
    }
    public function updateUser($req, $res){
	    $this->response_header();
		$input = $req->getParsedBody();
        $userdata = json_decode($req->getBody());
        $uploadedFiles = $req->getUploadedFiles();
        $dp_url = "";
        if(isset($uploadedFile['dp'])){
	        $uploadedImage = $uploadedFiles['dp'];
	        $file_name = 'dp'.'_'.uniqid();
	        $dp_url = "";
	        if ($uploadedImage !== null && $uploadedImage->getError() === UPLOAD_ERR_OK) {
		        $imageResp = $this->uploadImage($uploadedImage, $this->container->dp_directory."/".$file_name);
	            if($imageResp["error"]){
	                $output = ["statusCode" => 400, "message" => "Invalid DP type"];
	                return $this->encodeOutput($output);
	            }
	            $dp_url = $file_name.$imageResp["extension"];
	        }
        }
        User::where('id', $input['id'])->update([
            'name'  =>   $input['name'],
            'email' =>  $input['email'],
            'mobile'=>  $input['mobile'],
            'profile_pic'=> $dp_url
            ]);
        return $this->encodeOutput(["statusCode" => 200, "message" => "Updated Successfully"]);
       
    }
    public function videoUpload($req, $res){
        $this->response_header();
        $input = $req->getParsedBody();
        $userdata = json_decode($req->getBody());
        $uploadedFiles = $req->getUploadedFiles();
        $uploadedFile = $uploadedFiles['video'];

        if ($uploadedFile !== null && $uploadedFile->getError() === UPLOAD_ERR_OK) {
            $file_name = '_'.uniqid();
            //$uploadDirectory = $this->container->upload_directory;
            $videoResp = $this->moveUploadedFile_old($this->container->video_directory, $uploadedFile, $file_name);
            if($videoResp["error"]){
                $output = ["statusCode" => 400, "message" => "Invalid Video type","resp" => $videoResp];
                return $this->encodeOutput($output);
            }
            $video_url = $file_name.".".$videoResp["extension"];
            
            $thumbnail_url = $file_name.".jpg";
            $this->createThumbnail($this->container->video_directory."/".$video_url, $this->container->thumbnail_directory."/".$file_name.".jpg");
            
            $data = Commitment::saveData([
                'user_id'       =>  $input['user_id'],
                'video_url'     =>  $video_url,
                'header'        =>  $input['header'],
                'description'   =>  $input['description'],
                //'category'      =>  $input['video_type'],
                'your_llocation'=>  $input['your_llocation'],
                'submit_type'	=>  $input['video_type'],
                'status'        =>  1
                ]);
                if(empty($data)){
                    return $this->encodeOutput(["statusCode" => 200, "message" => "data not submitted"]);	
                }
                return $this->encodeOutput(["statusCode" => 200, "message" => "Video Added Successfully !!!"]);
        }
    }
    public function videoList($req, $res){
        $this->response_header();
        $id = $req->getAttribute('id');
        $data = Commitment::where('user_id',$id)->get()->toArray();
        if(empty($data)){
            return $this->encodeOutput(["statusCode" => 200, "message" => "user does not have videos",'data'=>$data]);
        }
        return $this->encodeOutput(["statusCode" => 200, "message" => "success",'data'=>$data]);
        
        /*$id = $req->getAttribute('id');
        $data = User::with('allVideos')->where('id',$id)->get()->toArray();
        return $this->encodeOutput(["statusCode" => 200, "message" => "Videos Data !!!",'data'=>$data]);*/
    }
    public function moveUploadedFile($directory, UploadedFile $uploadedFile)
    {
	    $this->response_header();
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
        $filename = sprintf('%s.%0.8s', $basename, $extension);

        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

        return $filename;
    }
    public function user_register($req, $res){
        $this->response_header();
        $input = $req->getParsedBody();
        $name = $this->getData($input, 'name');
        $email = $this->getData($input, 'email');
        $mobile = $this->getData($input, 'mobile');
        $password = $this->getData($input, 'password');
        $email =  User::select('email')->where(['email'=>$input['email']])->first();
            if(empty($email)){
                $userdata = User::saveData([
                    "name" => $input['name'],
                    "email" => $input['email'],
                    "mobile" =>$input['mobile'],
                    "password"=>password_hash($input['password'], PASSWORD_BCRYPT, ['cost' => 10]),
                    "token" =>bin2hex(openssl_random_pseudo_bytes(12))
                ]);
                return $this->encodeOutput(["statusCode" => 200, "message" => "Registration Successfully","data" => $userdata]);
            }else{
                return $this->encodeOutput(["statusCode" => 200, "message" => "This email already exits","data" => $email]);
            }
    }



    
    public function testFun($req, $res){
        return $this->view->render($response, 'test.html');
    }
    public function createUser($req, $res){
	    $this->response_header();
        $input = $req->getParsedBody();
        $device =$this->isMobile()? "mobile" : "web";
        $key = Activation::getToken(22,25);
        $values = [];
        if(isset($input["masterKey"]) && trim($input["masterKey"]) != '' && !is_nan($input["masterKey"])){
            $values["masterKey"] = $input["masterKey"];
        }
        $source = $this->getData($input, "utm_source");
        if($source){
            $values["source"] = $source;
        }

        $values["created_date"] = date('Y-m-d');
        $userKey = Activation::createUser($device, $values);

        $output = [
            "statusCode" => 200,
            "userKey" => $userKey,
            'dataKey' => $key
        ];
        return $this->encodeOutput($output);
    }

    public function clickupdater($req, $res, $args){
	    $this->response_header();
        $output = ["statusCode" => 400, "message" => self::INVALID_DATA];
        $userKey= $req->getAttribute('userKey');
        $input = $req->getParsedBody();

        $type = $this->getData($input, 'type');
        if($type == 'submitCommitmentClick'){
            Activation::clickUpdater($userKey, 'submit_commitment');
            $output = ["statusCode" => 200, "message" => "Success"];
        }
        else if($type == 'submitInitiativeClick'){
            Activation::clickUpdater($userKey, 'submit_initiative');
            $output = ["statusCode" => 200, "message" => "Success"];
        }
        /* else if($type == 'tShare'){
            User::where("userKey", $userKey)->update(['t_share' => 1]);
            $output = ["statusCode" => 200, "message" => "Success"];
        } */
        return $this->encodeOutput($output);
    }

    
    public function register($req, $res, $args){
	    $this->response_header();
        $output = ["statusCode" => 412, "message" => self::INVALID_DATA];
        $userKey= $req->getAttribute('userKey');
        ## $activationData= $req->getAttribute('activationData');
        $input = $req->getParsedBody();

        $name = $this->getData($input, 'name');
        $email = $this->getData($input, 'email');
        $mobile = $this->getData($input, 'mobile');
        $password = $this->getData($input, 'password');
        $category = $this->getData($input, 'category');
        $organizationName = $this->getData($input, 'organizationName');
        $submitType = $this->getData($input, 'submitType');
        $dp = $this->getData($input, 'dp');
        $lat = $this->getData($input, 'lat');
        $lng = $this->getData($input, 'lng');

        if(!$name){
            $output = ["statusCode" => 400, "message" => "Invalid Name"];
        }
        else if(!$email || !User::isEmail($email)){
            $output = ["statusCode" => 400, "message" => "Invalid Email ID"];
        }
        else if($mobile && !preg_match(self::MOBILE_REGEX, $mobile)){
            $output = ["statusCode" => 400, "message" => self::INVALID_MOBILE];
        }
        else if($category !== 'Individual' && $category !== 'Organization' ){
            $output = ["statusCode" => 400, "message" => "Invalid category"];
        }
        else if($category == 'Organization' && !$organizationName){
            $output = ["statusCode" => 400, "message" => "Invalid Organization Name"];
        }
        else if($submitType !== 'Commitment' && $submitType !== 'Initiative'){
            $output = ["statusCode" => 400, "message" => "Invalid submit type"];
        }
        // if(!$lat  || $lat > 90 || $lat < -90){
        //     $output["message"] = "Invalid latitude";
        //     return $this->encodeOutput($output);
        // }
        // else if(!$lng || $lng > 180 || $lng < -180){
        //     $output["message"] = "Invalid longitude";
        //     return $this->encodeOutput($output);
        // }

        if($output["statusCode"] != 412){
            return $this->encodeOutput($output);
        }
        $output["statusCode"] = 400;
        
        if($category == 'Individual'){    
            $organizationName = null;
        }

        $uploadedFiles = $req->getUploadedFiles();
        

        $uploadedFile = $uploadedFiles['video'];
        //var_dump($uploadedFile);
        if ($uploadedFile !== null && $uploadedFile->getError() === UPLOAD_ERR_OK) {
            $file_name = $userKey.'_'.uniqid();
            //$uploadDirectory = $this->container->upload_directory;
            $videoResp = $this->moveUploadedFile($this->container->video_directory, $uploadedFile, $file_name);
            if($videoResp["error"]){
                $output = ["statusCode" => 400, "message" => "Invalid Video type","resp" => $videoResp];
                return $this->encodeOutput($output);
            }
            $video_url = $file_name.".".$videoResp["extension"];
            $thumbnail_url = $file_name.".jpg";
            $this->createThumbnail($this->container->video_directory."/".$video_url, $this->container->thumbnail_directory."/".$file_name.".jpg");
            
            $uploadedImage = $uploadedFiles['dp'];
            if ($uploadedImage !== null && $uploadedImage->getError() === UPLOAD_ERR_OK) {
                $imageResp = $this->uploadImage($uploadedImage, $this->container->dp_directory."/".$file_name);
                if($imageResp["error"]){
                    $output = ["statusCode" => 400, "message" => "Invalid DP type"];
                    return $this->encodeOutput($output);
                }

                if(!$mobile){
                    $mobile = null;
                }
                
                $dp_url = $file_name.$imageResp["extension"];
                $userdata = User::where("email", $email)->first();
                if(empty($userdata)){
                    $userdata = User::saveData([
                        "name" => $name,
                        "email" => $email,
                        "mobile" => $mobile,
                        "password"=>$password
                    ]);
                }
                Commitment::saveData([
                    "userKey" => $userKey,
                    "user_id"=> $userdata["id"],
                    "name" => $name,
                    "email" => $email,
                    "mobile" => $mobile,
                    "category" => $category,
                    "submit_type" => $submitType,
                    "video_url"=> "videos/".$video_url,
                    "thumbnail_url" => "thumbnail/".$thumbnail_url,
                    "profile_pic_url" => "dp/".$dp_url,
                    "lat" => $lat,
                    "lng" => $lng,
                    "organization_name" => $organizationName,
                    "created_day" => date('Y-m-d')
                ]);
                $output = ["statusCode" => 200, "message" => "Success"];
            }
        }
        return $this->encodeOutput($output);
    }

    public function getCommitments($req, $res, $args){
	    $this->response_header();
        ## $activationData= $req->getAttribute('activationData');
        $input = $req->getParsedBody();

        $pageNo = $this->getData($input, 'pageNo');
        $submitType = $this->getData($input, 'submitType');
        if(is_nan($pageNo) || $pageNo == 0){
            $pageNo = 1;
        }
        else{
            $pageNo = intval($pageNo);
        }
        $skip = 0;
        $pagesize = 12;
        if($pageNo > 1){
            $skip = ($pageNo -1) * $pagesize;
        }
        $query = Commitment::select('id','profile_pic_url as dp', 'name', 'category', 'submit_type as submitType','organization_name as organizationName','likes','created_day as createdAt','video_url as video', 'thumbnail_url as thumbnail')
        ->where('status',1);
        if($submitType == "Commitment" || $submitType == "Initiative"){
            $query->where('submit_type',$submitType);
        }

        $commitments = $query->orderBy('created_at','DESC')
        ->skip($skip)
        ->take($pagesize)
        ->get();
        return $this->encodeOutput(["statusCode" => 200, "message" => "Success","data" => $commitments]);
    }

    public function likeCommitment($req, $res, $args){
	    $this->response_header();
        $output = ["statusCode" => 400, "message" =>"Invalid action"];
        
        $userKey= $req->getAttribute('userKey');
        $activationData= $req->getAttribute('activationData');
        $input = $req->getParsedBody();

        $cid = $this->getData($input, 'cid');
        $action = $this->getData($input, 'action');
        if($action == 'like' || $action == 'unlike'){
            $commitmentData = Commitment::where("id", $cid)->first();
            if(!empty($commitmentData)){
                $likeData = Like::where("masterKey", $activationData["masterKey"])->where("cid",$cid)->get();
                $likeCount = 0;
                $unlikeCount = 0;
                foreach($likeData as $data){
                    if($data["action_name"] == "like"){
                        $likeCount ++;
                    }
                    else{
                        $unlikeCount++;
                    }
                }

                if($action == 'like' && $likeCount <= $unlikeCount){
                    //Can like
                    Commitment::find($commitmentData["id"])->increment('likes',1);
                    Like::saveData([
                        "masterKey" => $activationData["masterKey"],
                        "cid" => $cid,
                        "action_name" => $action
                    ]);
                    $output = ["statusCode" => 200, "message" =>"Success"];
                }
                else if($action == 'unlike' && $unlikeCount < $likeCount){
                    //can unlike
                    if($commitmentData["likes"] > 0){
                        Commitment::find($commitmentData["id"])->decrement('likes',1);
                        Like::saveData([
                            "masterKey" => $activationData["masterKey"],
                            "cid" => $cid,
                            "action_name" => $action
                        ]);
                    }
                    $output = ["statusCode" => 200, "message" =>"Success"];
                }
            }
        }
        return $this->encodeOutput($output);
    }

    public function getCounts($req, $res){
	    $this->response_header();
        $groupsCommitmentCount = Commitment::where('category', 'Organization')->where('submit_type', 'Commitment')->where('status',1)->count();
        $groupsInitiativeCount = Commitment::where('category', 'Organization')->where('submit_type', 'Initiative')->where('status',1)->count();
        $individualCommitmentCount = Commitment::where('category', 'Individual')->where('submit_type', 'Commitment')->where('status',1)->count();
        $individualInitiativeCount = Commitment::where('category', 'Individual')->where('submit_type', 'Initiative')->where('status',1)->count();

        return $this->encodeOutput([
            "statusCode" => 200,
            "message" =>"Success",
            "counts" => [
                "commitments" => [
                    "individual" => $individualCommitmentCount,
                    "groups" => $groupsCommitmentCount
                ],
                "initiatives" => [
                    "individual" => $individualInitiativeCount,
                    "groups" => $groupsInitiativeCount
                ]
            ]
        ]);
    }

    public function getCommitmentLoc($req, $res){
	    $this->response_header();
        $commitments = Commitment::select('id','profile_pic_url as dp', 'name', 'category', 'submit_type as submitType','organization_name as organizationName','likes','created_day as createdAt','video_url as video', 'thumbnail_url as thumbnail', 'lat',"lng")
        ->where('status',1)
        ->orderBy('created_at','DESC')
        ->take(100)
        ->get();
        return $this->encodeOutput(["statusCode" => 200, "message" => "Success","data" => $commitments]);
    }

    public function searchCommitment($req, $res)
    {
        $input = $req->getParsedBody();
        $search = $this->getData($input, 'search');
        $data = [];
        if(strlen($search) >= 3){
            $data = User::select('id as uid','name')
            ->where('name','like',$search.'%')
            ->orderBy('created_at','DESC')
            ->take(5)
            ->get();
        }
        return $this->encodeOutput(["statusCode" => 200, "message" => "Success","data" => $data]);
    }

    public function getCommitmentsOfUsers($req, $res, $args){
	    $this->response_header();
        $input = $req->getParsedBody();
        $commitments = [];
        $uid = intval($this->getData($args, 'uid'));
        if($uid!== 0 && !is_nan($uid)){
            $commitments = Commitment::select('id','profile_pic_url as dp', 'name', 'category', 'submit_type as submitType','organization_name as organizationName','likes','created_day as createdAt','video_url as video', 'thumbnail_url as thumbnail', 'lat',"lng")
            ->where('status',1)
            ->where('user_id',$uid)
            ->get();
            $inif = 1;
        }
        return $this->encodeOutput(["statusCode" => 200, "message" => "Success","data" => $commitments]);
    }

    private function moveUploadedFile_old($directory, UploadedFile $uploadedFile, $file_name)
    {
        $has_err = true;
        $extList = [
            "video/mp4" => "mp4",
            "video/mov" => "mov",
            "video/quicktime" => "mov",
        ];
        $extension = "";//pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $type = $_FILES["video"]["type"];
        if(array_key_exists($type, $extList)){
            $extension = $extList[$type];
        }
        if($extension){
            $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $file_name.".".$extension);
            $has_err = false;
        }
        return ["extension" => $extension, "error" => $has_err,"file" => ""];
    }

    private function uploadImage(UploadedFile $uploadedFile, $file_name){
        $has_err = true;
        $extList = [
            "image/png" => ".png",
            "image/jpeg" => ".jpg",
            "image/jpg" => ".jpg",
        ];
        $extension = "";//pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $type = $_FILES["dp"]["type"];
        if(array_key_exists($type, $extList)){
            $extension = $extList[$type];
        }
        if($extension){
            $uploadedFile->moveTo($file_name.$extension);
            $has_err = false;
        }
        return ["extension" => $extension, "error" => $has_err];
    }

    private function createThumbnail($video_url, $thumbnail_url){
        shell_exec('ffmpeg -i '.$video_url.' -vf "select=eq(n\,20)" -vframes 1 '.$thumbnail_url);
    }

}