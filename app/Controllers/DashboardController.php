<?php

namespace App\Controllers;

use App\Models\Activation;
use App\Models\UserProfile;
use App\Models\Score;


class DashboardController extends Controller
{
    private $algo = 'aes-256-gcm';
    
    public function totalAppVisitsCount($req, $res, $args){
        $output = ["status" => 400];

        $key = Activation::htmlEncode($this->getData($args, 'key'));
        if($key == "nRqH0be1mkdGrilYVNMTs6Nl"){
                
            $count = Activation::count();
            $output = [
                "status" => 200,
                "chartType" => "i-count",
                "datasets" => [
                    [
                        "icon" => "users",
                        "title"=> 'Total app visits',
                        "value"=> number_format($count),
                    ]
                ]
            ];    
        }
        return json_encode($output);
    }

    public function uniqueVisitsCount($req, $res, $args){
        $output = ["status" => 400];

        $key = Activation::htmlEncode($args['key']);
        if($key == "kjmO5pw1zfovv70qLKtjdSac"){
                
            $count = Activation::whereRaw('userKey = masterKey')->count();
            $output = [
                "status" => 200,
                "chartType" => "i-count",
                "datasets" => [
                    [
                        "icon" => "user",
                        "title"=> 'Unique app visits',
                        "value"=> number_format($count),
                    ]
                ]
            ];    
        }
        return json_encode($output);
    }

    public function deviceDistributionCount($req, $res, $args){
        $output = ["status" => 400];

        $key = Activation::htmlEncode($args['key']);
        if($key == "wOCXUHpcfq9syVmpfL8CzxuU"){
                
            $users = Activation::selectRaw("device,count(device) as device_count")->groupBy('device')->orderBy('device')->get();
            $sum = 0;
            $mobile_count = 0;
            if($users){
                foreach($users as $user){
                    $sum += $user->device_count;
                    if($user->device == "mobile"){
                        $mobile_count = $user->device_count;
                    }
                }
            }
            $output = [
                "status" => 200,
                "chartType" => "count",
                "title"=> 'Device distribution',
                "value"=> "No Data"
            ];
            
            if($sum > 0){
                $mobile_prc = round(($mobile_count/$sum)*100);
                $desk_prc = 100 - $mobile_prc;

                $output = [
                    "status" => 200,
                    "chartType" => "i-count",
                    "datasets" => [
                        [
                            "icon" => "phone",
                            "title"=> 'Mobile Visits',
                            "value"=> $mobile_prc . '%'
                        ],
                        [
                            "icon" => "screen",
                            "title"=> 'Desktop Visits',
                            "value"=> $desk_prc.'%'
                        ]
                    ]
                ];  
            }   
        }
        return json_encode($output);
    }

    public function appReVisitsCount($req, $res, $args){
        $output = ["status" => 400];

        $key = Activation::htmlEncode($args['key']);
        if($key == "DS9lk10zbRt5ZDoq64ROvj60"){
                
            $count = Activation::whereRaw('userKey != masterKey')->count();
            $output = [
                "status" => 200,
                "chartType" => "i-count",
                "datasets" => [
                    [
                        "icon" => "refresh",
                        "title"=> 'App Revisits',
                        "value"=> number_format($count)
                    ]
                ]
            ];    
        }
        return json_encode($output);
    }

    public function totalUniqueUsers($req, $res, $args){
        $output = ["status" => 400];

        $key = Activation::htmlEncode($args['key']);
        if($key == "zQDFK0L94ZhZdFKLej2dOTZ5"){
            $users = Activation::selectRaw('created_date as c_date, COUNT(userKey) as t_user_count,sum(if(userKey = masterKey,1,0)) as u_user_count')->groupBy('c_date')->orderBy('c_date')->get();
        
            $labels = [];
            $totalUserCount = [];
            $uniqueUserCount = [];

            if($users){
                foreach($users as $user){
                    $labels[] = $user->c_date;
                    $totalUserCount[] = $user->t_user_count;
                    $uniqueUserCount[] = $user->u_user_count;
                }
            }

            $output = [
                "status" => 200,
                "chartType" => "line-chart",
                "labels" => $labels,
                "chartData"=> [
                    ["label" => 'Total users',"value" => $totalUserCount],
                    ["label" => 'Unique users',"value" => $uniqueUserCount]
                ],
                "parameters" => [
                    "title"=> 'Total and unique users',
                    "xLabelString"=> 'Date',
                    "yLabelString"=> 'Number of users'
                ]
            ];
        }
        return json_encode($output);
    }

    public function trafficHeatMap($req, $res, $args){
        $output = ["status" => 400];

        $key = Activation::htmlEncode($args['key']);
        if($key == "7y2ENHZT9h40fTTian4Oz0HV"){
                
            $users = Activation::selectRaw("created_day_hour ,count(created_day_hour) as created_day_hour_count")->groupBy('created_day_hour')->get();
            
            $time = [3,7,11,15,19,23];
            $labels = ["12am-4am","4am-8am","8am-12pm","12pm-4pm","4pm-8pm","8pm-12am"];
            $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            $short_day = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
            $data = [];
            foreach($days as $day){
                $data[] = array_fill(0,count($labels), 0);
            }

            foreach($users as $user){
                $created_day_hour = explode("-", $user->created_day_hour);
                $created_day = $created_day_hour[0];
                $created_hour = $created_day_hour[1];
                $d = array_search($created_day, $short_day);
                $t = floor($created_hour/4);
                $data[$d][$t] += $user->created_day_hour_count;
                
            }
            $maxval = 0;
            $minval = 0;
            for($i=0; $i<count($data); $i++){
                for($j=0; $j<count($data[$i]);$j++){
                    if(($i==0 && $j==0) || $data[$i][$j]<$minval){
                        $minval = $data;
                    }
                    if($maxval < $data[$i][$j]){
                        $maxval= $data[$i][$j];
                    }
                }    
            }
            $buckets = [];
            $new_maxval = 0;
            
            if($maxval == 0){
                $buckets = [15];
                $new_maxval = 15;
            }
            else if($maxval < 50){
                $buckets = [18, 35, 50];
                $new_maxval = 50;
            }else{
                $new_maxval = $maxval + (25 - ($maxval % 25));
                $buckets = [
                    round($new_maxval * 0.3),
                    round($new_maxval * 0.55),
                    round($new_maxval * 0.75),
                    round($new_maxval * 0.9),
                    round($new_maxval)
                ];
            }
            $final_Data = [];
            foreach($data as $row){
                $t_data = [];
                $bucket_no = 0;
                foreach($row as $val){
                    if($val <= $buckets[0]){
                        $bucket_no = 1;
                    }
                    else if($val <= $buckets[1]){
                        $bucket_no = 2;
                    }
                    else if($val <= $buckets[2]){
                        $bucket_no = 3;
                    }
                    else if($val <= $buckets[3]){
                        $bucket_no = 4;
                    }
                    else{
                        $bucket_no = 5;
                    }
                    $t_data[] = ["value"=>$val,"bucketNo"=>$bucket_no];
                }
                $final_Data[] = $t_data;
            }
            $bucket_legend = [];
            $lastVal = -1;
            foreach($buckets as $b){
                $bucket_legend[] = ($lastVal + 1) . " to " . $b;
                $lastVal = $b;
            }
            $output = [
                "status" => 200,
                "chartType" => "heat-map",
                "chartData"=> $final_Data,
                "parameters" => [
                    "title"=> 'Traffic heat map',
                    "legend"=>$bucket_legend,
                    "xLabels" => $labels,
                    "yLabels" => $days,
                ]
            ];
        }
        return json_encode($output);
    }

    ## ##################################### insight-count ######################################
    public function totalRegistration($req, $res, $args)
    {
        $output = ["status" => 400];
        $key = Activation::htmlEncode($this->getData($args, 'key'));
        if($key == "ZRVBnsI4QmeSyQYOUl3wc6A0"){
            $count = Activation::whereNotNull('profile_id')->count();
            $output = [
                "status" => 200,
                "chartType" => "insight-count",
                "title"=> 'Total Registration',
                "value"=> number_format($count)
            ];
        }
        return json_encode($output);
    }

    public function uniqueRegistration($req, $res, $args)
    {
        $output = ["status" => 400];
        $key = UserProfile::htmlEncode($this->getData($args, 'key'));
        if($key == "tETdJhMu4BUx7vuEhYN7JzuH"){
            $count = UserProfile::count();
            $output = [
                "status" => 200,
                "chartType" => "insight-count",
                "title"=> 'Unique Registration',
                "value"=> number_format($count)
            ];
        }
        return json_encode($output);
    }

    public function clickOnStartGame($req, $res, $args)
    {
        $output = ["status" => 400];
        $key = Activation::htmlEncode($this->getData($args, 'key'));
        if($key == "D9Idwy7gem0Irp1mnMVzN6nr"){
            $count = Activation::sum('start_game_click');
            $output = [
                "status" => 200,
                "chartType" => "insight-count",
                "title"=> 'Click on Start game',
                "value"=> number_format($count)
            ];
        }
        return json_encode($output);
    }

    public function clickOnHowToPlay($req, $res, $args)
    {
        $output = ["status" => 400];
        $key = Activation::htmlEncode($this->getData($args, 'key'));
        if($key == "5Nyt9lpCPMmYDn3fqwVe4u1T"){
            $count = Activation::sum('how_to_play_click');
            $output = [
                "status" => 200,
                "chartType" => "insight-count",
                "title"=> 'Click on How to play',
                "value"=> number_format($count)
            ];
        }
        return json_encode($output);
    }

    public function clickOnPlayAgain($req, $res, $args)
    {
        $output = ["status" => 400];
        $key = Activation::htmlEncode($this->getData($args, 'key'));
        if($key == "8uz3q6iVc4pHsSa5no8SlIOd"){
            $count = Activation::sum('play_again_click');
            $output = [
                "status" => 200,
                "chartType" => "insight-count",
                "title"=> 'Click on Play Again',
                "value"=> number_format($count)
            ];
        }
        return json_encode($output);
    }
    
    public function ClickOnHome($req, $res, $args)
    {
        $output = ["status" => 400];
        $key = Activation::htmlEncode($this->getData($args, 'key'));
        if($key == "8uz3q6iVc4pHsSa5no8SlIOd"){
            $count = Activation::sum('home_click');
            $output = [
                "status" => 200,
                "chartType" => "insight-count",
                "title"=> 'Click on Home',
                "value"=> number_format($count)
            ];
        }
        return json_encode($output);
    }
    ## ##################################### insight-count ######################################

    public function deviceDistribution($req, $res, $args){
        $output = ["status" => 400];
        $key = Activation::htmlEncode($args['key']);

        if($key == "aRvt3VX9dCFgieeAGCS6aqRm"){
            $users = Activation::selectRaw("if(device = 'web','desktop',device) as device_name ,count(device) as device_count")->groupBy('device')->orderBy('device_count','DESC')->get();
            $labels = [];
            $data = [];
            if($users){
                foreach($users as $user){
                    $labels[] = ucfirst($user->device_name);
                    $data[] = $user->device_count;
                }
            }
            $output = [
                "status" => 200,
                "chartType" => "half-donut-chart",
                "labels" => $labels,
                "chartData"=> $data,
                "parameters" => [
                    "title"=> 'Device distribution'
                ]
            ];
        }
        return json_encode($output);
    }

    public function browserDistribution($req, $res, $args){
        $output = ["status" => 400];

        $key = Activation::htmlEncode($args['key']);
        if($key == "Gcft8ZEyBOz0EyIwrCqUYuF0"){
                
            $users = Activation::selectRaw("browser ,count(browser) as browser_count")->groupBy('browser')->orderBy('browser_count','DESC')->get();
            $labels = [];
            $data = [];
            if($users){
                $i= 0;
                $sum = 0;
                foreach($users as $user){
                    if($i<3){
                        $labels[] = ucfirst($user->browser);
                        $data[] = $user->browser_count;
                    }else{
                        $sum += $user->browser_count;
                    }
                    $i++;
                }
                if($i > 3){
                    $labels[] = "Others";
                    $data[] = $sum;
                }
            }
            $output = [
                "status" => 200,
                "chartType" => "pie-chart",
                "labels" => $labels,
                "chartData"=> $data,
                "parameters" => [
                    "title"=> 'Browser distribution'
                ]
            ];
        }
        return json_encode($output);
    }

    public function osDistribution($req, $res, $args){
        $output = ["status" => 400];

        $key = Activation::htmlEncode($args['key']);
        if($key == "kFMf2Z1VTaykzn01fRe0O1FP"){
                
            $users = Activation::selectRaw("os ,count(os) as os_count")->groupBy('os')->orderBy('os_count','DESC')->get();
            $labels = [];
            $data = [];
            if($users){
                $i= 0;
                $sum = 0;
                foreach($users as $user){
                    if($i<3){
                        $labels[] = ucfirst($user->os);
                        $data[] = $user->os_count;
                    }else{
                        $sum += $user->os_count;
                    }
                    $i++;
                }
                if($i > 3){
                    $labels[] = "Others";
                    $data[] = $sum;
                }
            }
            $output = [
                "status" => 200,
                "chartType" => "pie-chart",
                "labels" => $labels,
                "chartData"=> $data,
                "parameters" => [
                    "title"=> 'Os distribution'
                ]
            ];
        }
        return json_encode($output);
    }

    public function dayWiseTraffic($req, $res, $args){
        $output = ["status" => 400];

        $key = Activation::htmlEncode($args['key']);
        if($key == "hMkTyDKa6nd2X6PU5mDydYm2"){
                
            $users = Activation::selectRaw("created_day ,count(created_day) as created_day_count")->groupBy('created_day')->orderBy('created_day_count','DESC')->get();
            $labels = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

            $data = array_fill(0,7,0);
            if($users){
                foreach($users as $user){
                    $pos = array_search($user->created_day, $labels);
                    if($pos!== false){
                        $data[$pos] = $user->created_day_count;
                    }
                }
            }
            $output = [
                "status" => 200,
                "chartType" => "horizontal-bar-chart",
                "labels" => $labels,
                "chartData"=> $data,
                "parameters" => [
                    "title"=> 'Day wise traffic',
                    "labelString"=> 'Number of users'
                ]
            ];
        }
        return json_encode($output);
    }

    public function timeWiseTraffic($req, $res, $args){
        $output = ["status" => 400];

        $key = Activation::htmlEncode($args['key']);
        if($key == "qVkU9Kk6YQ6qO7mn8Frdlld9"){
                
            $users = Activation::selectRaw("created_hour ,count(created_hour) as created_hour_count")->groupBy('created_hour')->get();
            
            
            $labels = ['12am-1am', '1am-2am', '2am-3am', '3am-4am', '4am-5am', '5am-6am', '6am-7am', '7am-8am', '8am-9am', '9am-10am', '10am-11am', '11am-12pm', '12pm-1pm', '1pm-2pm', '2pm-3pm', '3pm-4pm', '4pm-5pm', '5pm-6pm', '6pm-7pm', '7pm-8pm', '8pm-9pm', '9pm-10pm', '10pm-11pm', '11pm-12am'];
            $data = array_fill(0,24,0);

            if($users){
                foreach($users as $user){
                    $data[$user->created_hour] = $user->created_hour_count;    
                }
            }

            $output = [
                "status" => 200,
                "chartType" => "line-chart",
                "labels" => $labels,
                "chartData"=> [
                    ["label" => 'Total users',"value" => $data]
                ],
                "parameters" => [
                    "title"=> 'Time wise traffic',
                    "xLabelString"=> 'Time',
                    "yLabelString"=> 'Number of users'
                ]
            ];
        }
        return json_encode($output);
    }

    public function formFillChart($req, $res, $args){
        $output = ["status" => 400];

        $key = Activation::htmlEncode($args['key']);
        if($key == "4F7GRVGu8pPavTyB0AxCQT3g"){
                
            $start_date = date('Y-m-d', strtotime('-6 days'));
            
            $users = Activation::selectRaw('created_date as c_date, SUM(if(form_fill_type = 1,1,0)) as u_form_fill,SUM(if(form_fill_type > 0,1,0)) as t_form_fill')->where([['created_date','>=',$start_date]])->groupBy('c_date')->orderBy('c_date')->get();

            $labels = [];
            $u_data = [];
            $t_data = [];
            if($users){
                foreach($users as $user){
                    $labels[] = $user->c_date;
                    $u_data[] = $user->u_form_fill;
                    $t_data[] = $user->t_form_fill;
                }
            }
            $output = [
                "status" => 200,
                "chartType" => "bar-chart",
                "labels" => $labels,
                "chartData"=> [
                    ["label" => 'Unique form fill', "value" => $u_data],
                    ["label" => 'Total form fill', "value" => $t_data]
                ],
                "parameters" => [
                    "title"=> 'Form fill (in last 7 days)'
                ]
            ];
        }
        return json_encode($output);
    }

    public function registeredUserData($req, $res, $args)
    {
        $output = ["status" => 400];

        $key = UserProfile::htmlEncode($args['key']);
        if($key == "lDs6WB0oLhDTGZiYo4pl5ECy"){
            $userList = UserProfile::get();
            $decData = [];

            foreach($userList as $row)
            {
                $name = openssl_decrypt(
                    hex2bin($row->name),
                    $this->algo,
                    hex2bin($this->name_key),
                    OPENSSL_RAW_DATA,
                    UserProfile::getNameIv(),
                    hex2bin($row->name_tag)
                );

                $mobile = openssl_decrypt(
                    hex2bin($row->mobile),
                    $this->algo,
                    hex2bin($this->mobile_key),
                    OPENSSL_RAW_DATA,
                    UserProfile::getMobileIv(),
                    hex2bin($row->mobile_tag)
                );

                $email = openssl_decrypt(
                    hex2bin($row->email),
                    $this->algo,
                    hex2bin($this->email_key),
                    OPENSSL_RAW_DATA,
                    UserProfile::getEmailIv(),
                    hex2bin($row->email_tag)
                );

                $decData[] = [
                    "Name" => $name,
                    "Mobile" => $mobile,
                    "Email" => $email
                ];
            }
            $output = [
                "status" => 200,
                "data" => $decData
            ];
        }
        return json_encode($output);
    }
}