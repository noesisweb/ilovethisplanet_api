<?php
use App\Middleware\ValidateUserKeyMiddleware;
use App\Middleware\DecryptDataMiddleware;


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


$app->get('/hello', function ($request, $response, $args) {
    return $this->view->render($response, 'test.html');
})->setName('profile');

$app->group('/api',function(){
    $this->post('/users', 'UsersController:createUser');
    $this->post('/getCounts', 'UsersController:getCounts');
    $this->post('/users/getCommitmentLoc', 'UsersController:getCommitmentLoc');
    $this->post('/users/search', 'UsersController:searchCommitment');
    $this->post('/users/commitment[/{uid}]', 'UsersController:getCommitmentsOfUsers');

    $this->post('/login', 'UsersController:login'); 
    $this->post('/user-register', 'UsersController:user_register')->setName('user-register');
    $this->get('/get-user/{id}','UsersController:getUser');
    $this->post('/update-user','UsersController:updateUser');  

    $this->post('/video-upload','UsersController:videoUpload')->setName('video-upload'); 
    $this->get('/video-list/{id}','UsersController:videoList');
    
});
$app->group('',function(){
    $this->post('/users/action[/{userKey}]', 'UsersController:clickupdater');
    $this->post('/users/register[/{userKey}]', 'UsersController:register');
    $this->post('/users/getCommitments[/{userKey}]', 'UsersController:getCommitments');
    $this->post('/users/likeCommitment[/{userKey}]', 'UsersController:likeCommitment');
})
->add(new ValidateUserKeyMiddleware());